<?php
namespace App;

require __DIR__ . '/vendor/autoload.php';

use App\Controllers\AnimalController;

$method = $_REQUEST['method'];
// $json = $_REQUEST['json'];
$json = file_get_contents("php://input");
$data = json_decode($json);

$methods = [
    'addanimal' => 'App\animalAddHandler',
    'getanimal' => 'App\animalHandle',
    'updateanimal' => 'App\animalUpdateHandler',
    'deleteanimal' => 'App\animalDeleteHandler'
];

// if (isset($method)) {
//     echo 'Method ' . $method . ' doesn\'t exitst <br>';
//     var_dump(function_exists('App\\'. $methods[$method]));
// }
// else {
//     echo 'Абщхду';
// }

if (empty($methods[$method]) || !function_exists($methods[$method])) {
    http_response_code(405);
    include('errors/405.php');
    // var_dump(function_exists('App\\'. $methods[$method]));
    // echo 'Method ' . $method . ' doesn\'t exitst';
    return;
}

echo $methods[$method]($data);

function animalAddHandler($data) 
{
    $params = [
        'kit_or_pes' => $data->kit_or_pes,
        'nickname' => $data->nickname,
    ];
    $animalController = new AnimalController();
    $animalController->addAnimal($params);
    
} 

function animalHandle($data) {
    $params = [];
    $params = [
        'nickname' => $data->nickname
    ];
    $animalController = new AnimalController();
    $animals = $animalController->getAnimals($params);
    if($animals == []){
        echo "Nothing found";
    }
    else{
        header('Content-Type: application/json');
        echo json_encode($animals);
    }
    
}

function animalUpdateHandler($data) {
    $params = [
        'id' => $data->id,
        'kit_or_pes' => $data->kit_or_pes,
        'nickname' => $data->nickname,
    ];
    $animalController = new AnimalController();
    $animalController->updateAnimals($params);
}

function animalDeleteHandler($data) {
    $params = [
        'id' => $data->id,
    ];
    $animalController = new AnimalController();
    $animalController->deleteAnimals($params);
}