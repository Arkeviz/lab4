<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>405 Method not Allowed</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            padding: 0;
            margin: 0;
            background-color: #36393f;
        }
    </style>
</head>

<body>
    <img id="dog" src="https://http.cat/405.jpg" alt="Dog" height="600">
</body>

</html>