<?php

namespace App\Controllers;

use GuzzleHttp\Client;

use App\Controllers\DbController;
use \DB;


class AnimalController 
{
    const DB_NAME = 'animal';
    const DB_USER_TABLE = 'kit_and_pes';

    function getRandomDog(): string{
        $ch = curl_init();
        $url = "https://dog.ceo/api/breeds/image/random";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        $html = curl_exec($ch);
        curl_close($ch);
        $html = json_decode($html);
        return $html->message;
    }

    public function addAnimal(array $params = [])
    {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);
        if(empty($params['nickname'])){
            echo "Fill the nickname";
        }
        else if($params['kit_or_pes'] == 'kit' || $params['kit_or_pes'] == 'pes'){
            DB::insert(self::DB_USER_TABLE, $params);
            echo "{$params['kit_or_pes']} {$params['nickname']} was successfully added";
        }
        else{
            echo "'kit_or_pes' field value must be 'kit' or 'pes'";
        }
    }

    public function getAnimals(array $params = [])
    {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);
        $where = (isset($params['nickname'])) ? ' WHERE nickname LIKE \'%' . $params['nickname'] . '%\'' : '';
        return DB::query("SELECT * FROM " . self::DB_USER_TABLE . " {$where}");
    }

    public function updateAnimals(array $params = [])
    {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);
        if($params['kit_or_pes'] == 'kit' || $params['kit_or_pes'] == 'pes'){
            if(!empty($params['id']) && !empty($params['nickname'])){
                DB::update(self::DB_USER_TABLE, ['kit_or_pes' => $params['kit_or_pes'], 'nickname' => $params['nickname']], " id=%s", $params['id']);         
                echo "Animal with id:{$params['id']} was successfully update";
            }
            else{
                echo "Fill the fields";
            }
        }
        else{
            echo "'kit_or_pes' field value must be 'kit' or 'pes'";
        }
    }

    public function deleteAnimals(array $params = [])
    {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);
        DB::delete(self::DB_USER_TABLE, 'id=%s', $params['id']);
        echo "Animal with id:{$params['id']} was successfully deleted";
    }
}