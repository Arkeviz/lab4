<?php 
require __DIR__ . '/vendor/autoload.php';
use App\Controllers\AnimalController;

$animalController = new AnimalController();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Задание 4</title>
    <style>
        body{
            display: flex;
            margin: 0;
            padding: 0;
            justify-content: center;
            align-items: center;
            background-color: #36393f;
        }
    </style>
</head>
<body>
    <img id="dog" src="<?php echo $animalController->getRandomDog(); ?>" alt="Dog" height="600">
</body>
</html>